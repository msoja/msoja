$(window).bind("scroll", function() {
    var Wid = $(window).width();
    var hei = $(window).height();
    var s =Wid - Math.min(900, $(document).scrollTop());
    var h = hei - Math.min(900, $(document).scrollTop());
    $(".header-box").width(s).height(h);


    if ($(window).scrollTop() > h) {
        $('.navbar').addClass('top-nav-collapse fixed');

    } else {
        $('.navbar').removeClass('top-nav-collapse fixed');
    }

    var height = $(window).scrollTop();

    if(height  > h) {
        $('.skillbar').each(function(){
            $(this).find('.skillbar-bar').animate({
                width:$(this).attr('data-percent')
            },2000);
        });
    }
});


/*$(window).scroll(function() {
    if ($(".navbar").offset().top > 300) {
        $(".navbar").addClass("top-nav-collapse fixed");
    } else {
        $(".navbar").removeClass("top-nav-collapse fixed");
    }
});*/


$(window).bind('scroll', function () {

});



$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

$( ".sendd" ).click(function() {
    $( ".hidden-popup" ).show('fold');
});

$('.close').click(function(){
    $( ".hidden-popup" ).fadeOut();
});


$('.btn-popup').hover(function(){
    $(this).text("Wyslij");
}, function() {
    $(this).text("Gotowe ?");
});

$( ".localisation" ).click(function() {
    $( ".map" ).toggle( "slow" );
});


$(document).ready(function(){


    $('.caption').hide();
    $('.thumbnail').hover(function(){
            $(this).find('.caption').show('scale');
        },
        function(){
            $(this).find('.caption').hide("slide", { direction: "right" }, 1000);

    });


    $( '#submit' ).click( function () {
        event.preventDefault();
        $.ajax( {
            url: "send.php",
            data: {
                'name': $( '#name' ).val(),
                'email': $( '#email' ).val(),
                'message': $( '#message' ).val()
            },
            type: 'POST',
            success: function (  ) {
                $( '.send-message' ).html( 'wyslano' );
                $( '#name' ).val();
                $( '#email' ).val();
                $( '#message' ).val();
            },
            error: function (  ) {
                $( '.send-message' ).html( 'error' );
            }
        });
    });

});